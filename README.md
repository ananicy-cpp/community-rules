# Ananicy Cpp - Community rules

_WIP_

The goal of this repository is to hold the rules created by the community
for Ananicy Cpp or Ananicy (the original project in Python).

What needs to be done:

- [ ] Implement a clear benchmark protocol to prove the usefulness of the rule
- [ ] Write a guideline on how to propose a new rule
- [ ] Provide some examples
